export class AllFilmsDto {
  id: number;
  film_name: string;
  synopsis: string;
  vote_average: string;
  image: string;
  release_date: Date;
  genre: string;
}
