import { Module } from "@nestjs/common";
import { FilmsService } from "./films.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { FilmsEntity } from "./films.entity";

@Module({
  imports: [TypeOrmModule.forFeature([FilmsEntity])],
  exports: [TypeOrmModule, FilmsService],
  providers: [FilmsService],
})
export class FilmsModule {}
