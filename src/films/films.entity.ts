import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity("films")
export class FilmsEntity {
  @PrimaryColumn({ unique: true })
  id: number;
  @Column({ unique: true })
  title: string;
  @Column("text", { nullable: true })
  synopsis: string;
  @Column("datetime", { nullable: true })
  release_date: Date;
  @Column({ nullable: true })
  image: string;
  @Column({ nullable: true })
  genre: string;
  @Column({ nullable: true })
  vote_average: string;
  @Column("datetime", {
    default: () => "CURRENT_TIMESTAMP",
  })
  created_at: Date;
  @Column("datetime", {
    default: () => "CURRENT_TIMESTAMP()",
    onUpdate: "CURRENT_TIMESTAMP()",
  })
  updated_at: Date;
}
