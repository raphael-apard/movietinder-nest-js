import { Body, Controller, Get, Post, Req, Res } from "@nestjs/common";
import { FilmsService } from "./films.service";
import { Request, Response } from "express";
import { JwtService } from "@nestjs/jwt";
import * as dotenv from "dotenv";
import { AllFilmsDto } from "./dto/allFilms.dto";
import { NameFilmDto } from "./dto/nameFilm.dto";
dotenv.config();
@Controller("films")
export class FilmsController {
  constructor(
    private readonly filmsServices: FilmsService,
    private jwtServices: JwtService
  ) {}

  @Get()
  async getAllFilms(@Req() req: Request, @Res() res: Response) {
    try {
      const cookie = req.cookies["jwt"];
      const data = await this.jwtServices.verifyAsync(cookie);
      if (!data) {
        res.redirect("/login");
      }
      const dataFilms = await this.filmsServices.getFilmsApi();
      res.render("index.hbs", {
        films: dataFilms.data.results,
        name: data.name,
      });
    } catch (e) {
      res.redirect("/login");
    }
  }

  @Post("all-films")
  async addToDb(@Body() dataFilm: AllFilmsDto, @Res() res: Response) {
    const dataFilms = await this.filmsServices.getFilmsApi();
    try {
      const {
        film_name,
        synopsis,
        vote_average,
        image,
        release_date,
        genre,
        id,
      } = dataFilm;
      const result = await this.filmsServices.insertFilm(
        id,
        film_name,
        synopsis,
        release_date,
        image,
        genre,
        vote_average
      );
      if (result.affectedRows) {
        res.render("index.hbs", {
          films: dataFilms.data.results,
          message: "The movie has been added",
        });
      }
    } catch (e) {
      res.render("index.hbs", {
        films: dataFilms.data.results,
        message: "This movie was already added",
      });
    }
  }

  @Post()
  async getFilmsByName(@Body() nameFilms: NameFilmDto, @Res() res: Response) {
    try {
      const dataFilms = await this.filmsServices.getFilmsApiByName(
        nameFilms.films_name
      );
      res.render("index.hbs", { films: dataFilms.data.results });
    } catch (e) {
      res.render("index.hbs", { message: "Film not found" });
    }
  }
}
