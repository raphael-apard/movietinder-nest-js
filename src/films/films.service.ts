import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { FilmsEntity } from "./films.entity";
import * as dotenv from "dotenv";
import axios from "axios";
dotenv.config();

@Injectable()
export class FilmsService {
  constructor(
    @InjectRepository(FilmsEntity)
    private filmsRepository: Repository<FilmsEntity>
  ) {}

  findAll(): Promise<FilmsEntity[]> {
    return this.filmsRepository.find();
  }

  findOne(id: number): Promise<FilmsEntity> {
    return this.filmsRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.filmsRepository.delete(id);
  }

  async getFilmsApi(): Promise<any> {
    try {
      const response = await axios.get(
        process.env.API_URL_BASE +
          process.env.API_DICOVER_URL +
          process.env.API_KEY +
          process.env.API_LANG
      );
      return response;
    } catch (err) {
      return "Something has gone wrong";
    }
  }
  async getGender(): Promise<any> {
    try {
      const response = await axios.get(
        process.env.API_URL_BASE +
          process.env.API_GENRE +
          process.env.API_KEY +
          process.env.API_LANG
      );
      return response;
    } catch (err) {
      return "Something has gone wrong";
    }
  }

  async getFilmsApiByName(filmName: string): Promise<any> {
    try {
      const response = await axios.get(
        process.env.API_URL_BASE +
          process.env.API_SEARCH +
          process.env.API_KEY +
          process.env.API_LANG +
          process.env.API_QUERY +
          filmName
      );
      return response;
    } catch (err) {
      return "Something has gone wrong";
    }
  }

  async insertFilm(
    id: number,
    title: string,
    synopsis: string,
    release_date: Date,
    image: string,
    genre: string,
    vote_average: string
  ): Promise<any> {
    return this.filmsRepository.query(
      "INSERT INTO films(id, title, synopsis, release_date, image, genre, vote_average) VALUES (?,?,?,?,?,?,?)",
      [id, title, synopsis, release_date, image, genre, vote_average]
    );
  }
}
