import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity("users")
export class UsersEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  first_name: string;
  @Column()
  last_name: string;
  @Column()
  email: string;
  @Column()
  password: string;
  @Column({ default: "client" })
  role: string;
  @Column("datetime", {
    default: () => "CURRENT_TIMESTAMP",
  })
  created_at: Date;
  @Column("datetime", {
    default: () => "CURRENT_TIMESTAMP()",
    onUpdate: "CURRENT_TIMESTAMP()",
  })
  updated_at: Date;
  @Column({ default: false })
  isActive: boolean;
  @Column({ default: true })
  status: boolean;
}
