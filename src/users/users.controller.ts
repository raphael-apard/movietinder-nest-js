import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Render,
  Req,
  Res,
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { JwtService } from "@nestjs/jwt";
import { Request, Response } from "express";

@Controller("users")
export class UsersController {
  constructor(
    private readonly usersServices: UsersService,
    private jwtServices: JwtService
  ) {}

  @Get()
  async getAllUsers(@Req() req: Request, @Res() res: Response) {
    try {
      const cookie = req.cookies["jwt"];
      const data = await this.jwtServices.verifyAsync(cookie);
      if (!data) {
        res.redirect("/login");
      }
      res.render("user.hbs", { user: await this.usersServices.findAll() });
    } catch (e) {
      res.redirect("/login");
    }
  }

  @Get("/:id")
  async deleteUser(@Res() res: Response, @Req() req: Request, @Param("id") id) {
    const userRemoved = await this.usersServices.remove(id);
    const { affected } = userRemoved;
    if (affected) {
      res.redirect("/users");
    }
  }
}
