import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UsersEntity } from "./users.entity";
import { DeleteResult } from "typeorm/query-builder/result/DeleteResult";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersEntity)
    private usersRepository: Repository<UsersEntity>
  ) {}

  findAll(): Promise<UsersEntity[]> {
    return this.usersRepository.find();
  }

  async findOne(email: any): Promise<UsersEntity> {
    return this.usersRepository.findOne(email);
  }

  findById(id: number): Promise<UsersEntity> {
    return this.usersRepository.findOne(id);
  }

  async remove(id: number): Promise<any> {
    return await this.usersRepository.delete(id);
  }
}
