export class GetUsersDto {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  role: string;
  createdOn: Date;
  updatedOn: Date;
  active: boolean;
}
