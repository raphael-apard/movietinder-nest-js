import {
  Body,
  Controller,
  Get,
  Post,
  BadGatewayException,
  Res,
  Req,
} from "@nestjs/common";
import { UsersService } from "./users/users.service";
import * as bcrypt from "bcrypt";
import { JwtService } from "@nestjs/jwt";
import { Response, Request } from "express";

@Controller()
export class AppController {
  constructor(
    private readonly userServices: UsersService,
    private jwtServices: JwtService
  ) {}

  @Get("login")
  async loginPage(@Res() res: Response, @Req() req: Request) {
    try {
      const cookie = req.cookies["jwt"];
      const data = await this.jwtServices.verifyAsync(cookie);
      if (!data) {
        res.render("login.hbs");
      }
      res.redirect("/films");
    } catch (e) {
      res.render("login.hbs");
    }
  }

  @Post("login")
  async getLoginData(
    @Body("email") email: string,
    @Body("password") passwd: string,
    @Res({ passthrough: true }) res: Response
  ) {
    try {
      const user = await this.userServices.findOne({ email });
      if (!user) {
        throw new BadGatewayException("Invalid credentials");
      }
      if (!(await bcrypt.compare(passwd, user.password))) {
        throw new BadGatewayException("Invalid credentials");
      }

      const jwt = await this.jwtServices.signAsync({
        id: user.id,
        name: user.first_name,
      });
      res.cookie("jwt", jwt, { httpOnly: true });
      res.redirect("/films");
    } catch (e) {
      // res.redirect("/login"); //todo: change, if the credentials are incorrect, then render login.hbs with a message
      throw new BadGatewayException("Invalid credentials");
    }
  }

  @Post("/logout")
  async logout(@Res({ passthrough: true }) res: Response) {
    res.clearCookie("jwt");

    res.redirect("/login");
  }
}
