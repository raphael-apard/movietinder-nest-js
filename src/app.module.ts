import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { FilmsController } from "./films/films.controller";
import { FilmsModule } from "./films/films.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UsersModule } from "./users/users.module";
import { UsersController } from "./users/users.controller";
import { JwtModule } from "@nestjs/jwt";
import * as dotenv from "dotenv";
dotenv.config();
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    JwtModule.register({
      secret: process.env.SECRET, //une phrase, ce que vous voulez
      signOptions: { expiresIn: process.env.EXPIRATION }, //Le temps d'expiration du login, ex: 1h
    }),
    UsersModule,
    FilmsModule,
  ],
  controllers: [AppController, UsersController, FilmsController],
  providers: [AppService],
})
export class AppModule {}
