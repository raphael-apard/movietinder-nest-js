# MovieTinder - Nest JS

## Getting started

1. npm i
2. Création du fichier `ormconfig.json` avec les données de `example.ormconfig.json`
3. Créer manuellement l'utilisateur admin dans la base de données
   (Définir un mot de passe haché dans la base de données, sur https://bcrypt-generator.com/, vous pouvez hacher votre mot de passe en ligne, et coller le mot de passe dans le db)
4. Avant de lancer le serveur, vous devriez jeter un coup d'oeil au fichier `.env.example`, pour configurer l'api
5. npm start
6. http://localhost:3000

## DONE

#### ETQ admin je devrais pouvoir me connecter

done

#### ETQ admin afficher la liste de tous les utilisateurs

done

#### ETQ admin je veux pouvoir supprimer un utilisateur

done

#### Ajouter des films à la base de données avec une api

done

## TODO

#### ETQ admin, j'accède à la navigation via une navbar

todo

#### ETQ admin je veux voir la liste des recommandations

todo

#### ETQ admin je veux pouvoir bloquer et debloquer un utilisateur

todo --> Moha

#### ETQ admin je veux pouvoir editer un utilisateur

todo

#### ETQ admin je veux pouvoir rechercher un utilisateur par nom et par email

todo

#### Une page d'enregistrement momentanée, pour créer un compte.

todo --> Musta
